var rules = "<span style='background-color:black'> WELCOME! TO MY X AND O GAME!<br/> THIS GANE IS 1 BY 1 GAME <br/> EVERY PLAYER NEEDS TO COMPLETE A SEQUENCE FOR HIS OWN MARK (X OR O). <br/><br/> YOU HAVE THE ABILLITY TO CHANGE THE SEQUENCE SIZE <br/> THE BOARD SIZE, THE PLAYERS COLORS AND NAMES <br/> AND EVEN ADD MUSIC! <br/> THE CREATOR OF THE GAME IS DAN AHUV <br/> ENJOY! </span>"
var rows = 3, cols = 3, run = 3;
var player = 0;
var p1Color = "black", p2Color = "white", p1In = "white", p2In = "black";
var p1Name = "Player 1", p2Name = "Player 2";
var p1Points = 0, p2Points = 0;

function START() {
    document.getElementById("title").innerHTML = ("");
    document.getElementById("button_game").innerHTML = ("<button onclick='START()' class='b'>NEW GAME</button><button onclick='RESET()' class='b'>POINTS SET</button><button onclick='MENU()' class='b'>MENU</button>");
    document.getElementById("button_rules").innerHTML = ("");
    document.getElementById("button_settings").innerHTML = ('<span class="points" style="color: ' + p1In + '; background-color:' + p1Color + ';">' + p1Name + ' POINTS: ' + p1Points + '</span ><br/><br/><span class="points" style="color: ' + p2In + '; background-color:' + p2Color + ';">' + p2Name + ' POINTS: ' + p2Points + '</span>');
    player = 0;
    BUILD_BOARD();
}
function RULES() {
    document.getElementById("title").innerHTML = ("");
    document.getElementById("button_game").innerHTML = ("<button onclick='MENU()' class='b'>MENU</button>");
    document.getElementById("button_rules").innerHTML = ("<div class='rules'>" + rules + "</div>");
    document.getElementById("button_settings").innerHTML = ("");
}
function SETTINGS() {
    document.getElementById("title").innerHTML = ("");
    document.getElementById("button_game").innerHTML = ("<button onclick='MENU()' class='b'>MENU</button><br/><button onclick='SOUND()' class='b'>SOUND</button><br/><button onclick='BOARD_SIZE()' class='b'>BOARD</button><br/><button onclick='COLORS()' class='b'>COLORS</button><br/><button onclick='NAMES()' class='b'>NAMES</button>");
    document.getElementById("button_rules").innerHTML = ("");
    document.getElementById("button_settings").innerHTML = ("");
}
function MENU() {
    document.getElementById("title").innerHTML = ("X & O");
    document.getElementById("button_game").innerHTML = ("<button onclick='START()' class='a'>PLAY</button>");
    document.getElementById("button_rules").innerHTML = ("<button onclick='RULES()' class='a'>RULES</button>");
    document.getElementById("button_settings").innerHTML = ("<button onclick='SETTINGS()' class='a'>SETTINGS</button>");
}
function RESET() {
    p1Points = 0, p2Points = 0;
    START();
}
function SOUND() {
    document.getElementById("button_game").innerHTML = ('<button onclick="SETTINGS()" class="b">BACK</button><br/> <select id="sound" onchange="S_CONTROL(this)"> <option value = "calm"> Calm Music</option ><option value="battle">Battle Music</option><option value="old">Old Music</option><option value="none">No Music</option></select >');
}
function S_CONTROL(q) {
    var song = document.getElementById("audio");
    if (q.value == "calm") {
        song.src = "s1.mp3"
    }
    if (q.value == "battle") {
        song.src = "s2.mp3"
    }
    if (q.value == "old") {
        song.src="s3.mp3"
    }
    if (q.value == "none") {
        song.src = ""
    }
}
function BOARD_SIZE() {
    document.getElementById("button_game").innerHTML = ('<button onclick="SETTINGS()" class="b">BACK</button><br/><div class="rules" > BOARD ROWS:</div > <br /><select onchange="ROWS_CHECK(this)"><option value = "3" > 3</option ><option value = "4" > 4</option ><option value = "5" > 5</option ><option value = "6" > 6</option ><option value = "7" > 7</option ><option value = "8" > 8</option ><option value = "9" > 9</option ></select > ');
    document.getElementById("button_rules").innerHTML = ('<div class="rules" > BOARD COLUMNS:</div > <br /><select onchange="COLS_CHECK(this)"><option value = "3" > 3</option ><option value = "4" > 4</option ><option value = "5" > 5</option ><option value = "6" > 6</option ><option value = "7" > 7</option ><option value = "8" > 8</option ><option value = "9" > 9</option ></select >');
    document.getElementById("button_settings").innerHTML = ('<div class="rules" > SEQUENCE LENGTH:</div > <br /><select onchange="RUN_CHECK(this)"><option value = "3"> 3</option ><option value = "4" > 4</option ><option value = "5" > 5</option ><option value = "6" > 6</option ><option value = "7" > 7</option ><option value = "8" > 8</option ><option value = "9" > 9</option ></select >');
}
function ROWS_CHECK(q) {
    rows = q.value;
}
function COLS_CHECK(q) {
    cols = q.value;
}
function RUN_CHECK(q) {
    run_check = q.value
    if (run_check > cols || run_check > rows) {
        alert("The Sequence Cannot be bigger then the board, reset the Sequence to 3");
        run = 3;
        q.selectedIndex = q.options[1];
    }
    else {
        run = run_check;
    }
}
function COLORS() {
    document.getElementById("button_game").innerHTML = ('<button onclick="SETTINGS()" class="b">BACK</button><br/><div class="rules" >PLAYER 1 COLOR</div > <br /><select onchange="PLAYER1_COLOR(this)"><option value = "black" >Black</option ><option value = "white" >White</option ><option value = "red" >Red</option ><option value = "blue" >Blue</option ><option value = "yellow" >Yellow</option ><option value = "green" >Green</option ><option value = "orange" >Orange</option ><option value = "pink" >Pink</option ><option value = "purple" >Purple</option ></select >');
    document.getElementById("button_rules").innerHTML = ('<div class="rules" >PLAYER 2 COLOR</div > <br /><select onchange="PLAYER2_COLOR(this)" id="selectColor2"><option value = "black" >Black</option ><option value = "white" >White</option ><option value = "red" >Red</option ><option value = "blue" >Blue</option ><option value = "yellow" >Yellow</option ><option value = "green" >Green</option ><option value = "orange" >Orange</option ><option value = "pink" >Pink</option ><option value = "purple" >Purple</option ></select >');
    document.getElementById("selectColor2").selectedIndex = "1";
}
function PLAYER1_COLOR(q) {
    if (q.value == p2Color) {
        alert("Same color cannot be chosen.");
        if (p2Color == "black") {
            q.selectedIndex = "1"
            p1Color = "white"
        }
        else {
            q.selectedIndex = "0"
            p1Color = "black"
        }
    }
    else {
        p1Color = q.value;
    }
    if (p1Color == "black") {
        p1In = "white"
    }
    else {
        p1In = "black"
    }
}
function PLAYER2_COLOR(q) {
    if (q.value == p1Color) {
        alert("Same color cannot be chosen.");
        if (p1Color == "black") {
            q.selectedIndex = "1"
            p2Color = "white"
        }
        else {
            q.selectedIndex = "0"
            p2Color = "black"
        }
    }
    else {
        p2Color = q.value;
    }
    if (p2Color == "black") {
        p2In = "white"
    }
    else {
        p2In = "black"
    }
}
function NAMES() {
    document.getElementById("button_game").innerHTML = ('<button onclick="SETTINGS()" class="b">BACK</button><br/><div class="rules" >PLAYER 1 NAME</div > <br /><input id="name1"><br/><br/><div class="rules" >PLAYER 2 NAME</div ><br/><input id="name2"><br/><br/><input type="submit" onclick="NAME1(this)" value="LOAD">');
}
function NAME1(q) {
    name1 = document.getElementById("name1").value;
    name2 = document.getElementById("name2").value;
    if (name1 == name2) {
        alert("Names Cannot be the same")
        p1Name = "Player 1", p2Name = "Player 2";
    }
    else {
        p1Name = name1, p2Name = name2;
    }
    if (p1Name == "") {
        p1Name = "Player 1"
    }
    if (p2Name == "") {
        p2Name = "Player 2"
    }
}
function BUILD_BOARD() {
    var r = 0, c;
    var table = "<table>";
    var id = 0;
    while (r < rows) {
        c = 0;
        table += "<tr>"
        while (c < cols) {
            table += "<td id=" + id.toString() + " onclick='TD_CLICK(this)' style='color: black; background-color:gray;'></td>"
            id++;
            c++;
        }
        r++;
        table += "</tr>"
    }
    table += "</table>";
    document.getElementById("button_rules").innerHTML = (table);
}
function TD_CLICK(q) {
    var stop = 0;
    var flag = 0;
    var tie = 0;
    var id = q.id;
    var gold_an = gold_an = [-1, -1, -1, -1, -1, -1, -1, -1, -1]

    player = player % 2;
    var rowClicked = Math.floor(id / cols);
    var colClicked = id % cols;
    if (document.getElementById(id).style.backgroundColor == "gray") {
        if (player == 0) {
            document.getElementById(id).style.backgroundColor = p1Color;
            document.getElementById(id).style.color = p1In;
            document.getElementById(id).style.fontSize = "40px";
            document.getElementById(id).innerHTML = "X";
        }
        else {
            document.getElementById(id).style.backgroundColor = p2Color;
            document.getElementById(id).style.color = p2In;
            document.getElementById(id).style.fontSize = "40px";
            document.getElementById(id).innerHTML = "O";
        }
        player++;

        var colorClicked = document.getElementById(id).style.backgroundColor;

        var i = 0;
        var r = rowClicked;
        var c = colClicked;
        var win = 0;
        while (c < cols && win != run) {
            idTdTocheck = r * cols + c;
            colorTdToCheck = document.getElementById(idTdTocheck).style.backgroundColor;
            if (colorClicked !== colorTdToCheck) {
                break;
            }
            gold_an[i] = idTdTocheck;
            win++;
            c++;
            i++;
        }
        c = colClicked - 1;
        while (c > -1 && win != run) {
            idTdTocheck = r * cols + c;
            colorTdToCheck = document.getElementById(idTdTocheck).style.backgroundColor;
            if (colorClicked !== colorTdToCheck) {
                break;
            }
            gold_an[i] = idTdTocheck;
            win++;
            c--;
            i++;
        }
        if (win == run) {
            stop = 1;
        }
        if (stop == 0) {
            i = 0;
            gold_an = gold_an = [-1, -1, -1, -1, -1, -1, -1, -1, -1]
            r = rowClicked;
            c = colClicked;
            win = 0;
            while (r < rows && win != run) {
                idTdTocheck = r * cols + c;
                colorTdToCheck = document.getElementById(idTdTocheck).style.backgroundColor;
                if (colorClicked !== colorTdToCheck) {
                    break;
                }
                gold_an[i] = idTdTocheck;
                i++;
                r++;
                win++;
            }
            r = rowClicked - 1;
            while (r > -1 && win != run) {
                idTdTocheck = r * cols + c;
                colorTdToCheck = document.getElementById(idTdTocheck).style.backgroundColor;
                if (colorClicked !== colorTdToCheck) {
                    break;
                }
                gold_an[i] = idTdTocheck;
                i++;
                win++;
                r--;
            }
            if (win == run) {
                stop = 1;
            }
        }
        if (stop == 0) {
            i = 0;
            gold_an = gold_an = [-1, -1, -1, -1, -1, -1, -1, -1, -1]
            r = rowClicked;
            c = colClicked;
            win = 0;
            while (c < cols && r < rows && win != run) {
                idTdTocheck = r * cols + c;
                colorTdToCheck = document.getElementById(idTdTocheck).style.backgroundColor;
                if (colorClicked !== colorTdToCheck) {
                    break;
                }
                gold_an[i] = idTdTocheck;
                i++;
                r++;
                c++;
                win++;
            }
            r = rowClicked - 1;
            c = colClicked - 1;
            while (r > -1 && c > -1 && win != run) {
                idTdTocheck = r * cols + c;
                colorTdToCheck = document.getElementById(idTdTocheck).style.backgroundColor;
                if (colorClicked !== colorTdToCheck) {
                    break;
                }
                gold_an[i] = idTdTocheck;
                i++;
                win++;
                r--;
                c--;
            }
            if (win == run) {
                stop = 1;
            }
        }
        if (stop == 0) {
            i = 0;
            gold_an = gold_an = [-1, -1, -1, -1, -1, -1, -1, -1, -1]
            r = rowClicked;
            c = colClicked;
            win = 0;
            while (c >= 0 && r < rows && win != run) {
                idTdTocheck = r * cols + c;
                colorTdToCheck = document.getElementById(idTdTocheck).style.backgroundColor;
                if (colorClicked !== colorTdToCheck) {
                    break;
                }
                gold_an[i] = idTdTocheck;
                i++;
                r++;
                c--;
                win++;
            }
            r = rowClicked - 1;
            c = colClicked + 1;
            while (c < cols && r >= 0 && win != run) {
                idTdTocheck = r * cols + c;
                colorTdToCheck = document.getElementById(idTdTocheck).style.backgroundColor;
                if (colorClicked !== colorTdToCheck) {
                    break;
                }
                gold_an[i] = idTdTocheck;
                i++;
                r--;
                c++;
                win++;
            }
            if (win == run) {
                stop = 1;
            }
        }
        if (stop == 0) {
            var board_index = rows * cols;
            i = 0;
            while (i < board_index) {
                if (document.getElementById(i).style.backgroundColor != "gray") {
                    i++;
                }
                else {
                    flag = 1;
                    break;
                }
            }
            if (flag == 0) {
                alert("TIE")
                stop = 1;
                tie = 1;
            }
        }
        if (stop == 1) {
            if (tie == 0) {
                i = 0;
                for (i = 0; i < 9; i++) {
                    if (gold_an[i] == -1) {
                        break;
                    }
                    document.getElementById(gold_an[i]).classList.add("gold");
                }
                if (player == 1) {
                    alert(p1Name + " WON")
                    p1Points++;
                }
                else {
                    alert(p2Name + " WON")
                    p2Points++;
                }
            }
            setTimeout(() => START(), 2000)
        }
    }
}